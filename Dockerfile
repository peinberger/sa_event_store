FROM peeinboerg/saabaseimage
MAINTAINER Peter Einberger <peter.einberger@ipa.fraunhofer.de>

COPY * /opt/event-store/

WORKDIR /opt/event-store/
RUN mvn assembly:assembly

CMD java -jar /opt/event-store/target/sa_event_store-0.0.1-SNAPSHOT-jar-with-dependencies.jar -debug -address localhost