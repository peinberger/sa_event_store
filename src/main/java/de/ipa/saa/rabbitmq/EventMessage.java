package de.ipa.saa.rabbitmq;

public class EventMessage {
	private String eventType, eventContent, eventSender;
	
	public EventMessage(){
		
	}
	
	public EventMessage(String eventSender, String eventType, String eventContent){
		this.eventType = eventType;
		this.eventContent = eventContent;
		this.eventSender = eventSender;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventContent() {
		return eventContent;
	}

	public void setEventContent(String eventContent) {
		this.eventContent = eventContent;
	}

	public String getEventSender() {
		return eventSender;
	}

	public void setEventSender(String eventSender) {
		this.eventSender = eventSender;
	}
}
