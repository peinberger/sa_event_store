package de.ipa.saa.eventStore;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

// Http 1.1 REST Implementation
public class ComProtocol {
	private Gson gson = new Gson();
	
	public void sendPOST(String uri, String path, String json) {
		Client client = ClientBuilder.newClient();
		WebTarget sender = client.target(uri).path(path);
		sender.request(MediaType.TEXT_HTML).post(Entity.json(json));
		client.close();
	}
	
	public void sendGET(String uri, String path) {
		Client client = ClientBuilder.newClient();
		WebTarget sender = client.target(uri).path(path);
		sender.request(MediaType.TEXT_HTML).get();
		client.close();
	}
	
	// Messaging Dummy
	public void sendMsg(){
		
	}

}
