package de.ipa.saa.eventStore;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.concurrent.TimeoutException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;

import com.google.gson.Gson;
import com.rabbitmq.client.*;

import de.ipa.saa.eventStore.CommandLineInterfaceHelper;
import de.ipa.saa.eventStore.Main;
import de.ipa.saa.eventStore.Singleton;
import de.ipa.saa.rabbitmq.EventMessage;

public class Main {
	private static Boolean DEBUG = true;
	public static String zookeeperPath;
	DataBase db = Singleton.getDataBaseInstance();
	static Gson gson = new Gson();
	
	public static void main(String[] args) {
		for(String u : args){
            System.out.println("Argument: "+u.toString());
        }
	     
        try {
            CommandLine cli = new CommandLineInterfaceHelper().create(args);
            
            if( cli.hasOption("debug")){
            	DEBUG = true;
            } else {
            	DEBUG = true;
            }
            
            // specify path of the loader component or if not: set it to src/main/resources/rules/
            
            if( cli.hasOption( "address" ) ) {
                zookeeperPath = cli.getOptionValue( "address" );
                if(DEBUG)System.out.println("zookeeperPath set to: "+zookeeperPath);
            } else {
                zookeeperPath =  "localhost";
                if(DEBUG)System.out.println("zookeeperPath set to: "+zookeeperPath);
            }
   
        } catch (ParseException e) {
            System.err.println( "Parsing failed.  Reason: " + e.getMessage() );
        }
		
		try {
			Main core = Singleton.getCoreInstance();
			core.init("eventStore", zookeeperPath);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	public void init(String queueName, String address) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(address);
		factory.setUsername("senseandact");
		factory.setPassword("Logistik11");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.queueDeclare(queueName, false, false, false, null);
		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				System.out.println("Message received: "+message);
				EventMessage eventMsg = gson.fromJson(message, EventMessage.class);
				Event event = new Event(eventMsg.getEventSender(),"bla", eventMsg.getEventType(), eventMsg.getEventContent(), LocalDateTime.now().toString());
				db.addEvent(event);
				
				System.out.println(" [x] Received '" + message + "'");
			}
		};
		channel.basicConsume(queueName, true, consumer);
	}

}
