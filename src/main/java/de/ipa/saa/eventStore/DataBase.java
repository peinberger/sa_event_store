package de.ipa.saa.eventStore;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.*;

import com.google.gson.Gson;

public class DataBase {

	Gson gson = new Gson();
	String dataBasePath = "jdbc:sqlite:src/main/java/ipa150/pe/EventStore/eventStore.db";
	Logger logger = Logger.getLogger("SystemLog");
	Logger difference = Logger.getLogger("TimeDifLog");
	FileHandler fh, fh2;
	
	public DataBase() {
		  try {  

		        // This block configure the logger with handler and formatter  
		        fh = new FileHandler("EventStore.log");
		        fh2 = new FileHandler("TimeDifLog.log");  
		        logger.addHandler(fh);
		        difference.addHandler(fh2);
		        SimpleFormatter formatter = new SimpleFormatter();  
		        fh.setFormatter(formatter);  
		        fh2.setFormatter(formatter);
		        
		        // the following statement is used to log any messages  
		        logger.info("Logger Started:");
		        difference.info("Logger Started:");

		    } catch (SecurityException e) {  
		        e.printStackTrace();  
		    } catch (IOException e) {  
		        e.printStackTrace();  
		    }  

	}
	
	public void addEvent(Event event) {
		logger.info(event.getEventReceivedTime()+" ## "+event.getEventType()+" ## "+event.getEventOccuredTime()+" ## "+event.getService()+" ## "+event.getSender()+" ## "+event.getSenderAddress()+" ## "+event.getEventContent());
		if (event.getEventType().equals("tester difference")){
			difference.info(event.getEventContent());
		}
		/**
		Connection c = null;
		if (event != null) {
			try {
				if (c == null){
					Class.forName("org.sqlite.JDBC");
					c = DriverManager.getConnection(dataBasePath);
					c.setAutoCommit(true);
				}
				
				PreparedStatement stm = c.prepareStatement(
						"INSERT INTO events (eventReceivedTime, eventOccuredTime, service, sender, senderAddress, eventType, eventContent) VALUES (?,?,?,?,?,?,?)");
				stm.setString(1, event.getEventReceivedTime());
				stm.setString(2, event.getEventOccuredTime());
				stm.setString(3, event.getService());
				stm.setString(4, event.getSender());
				stm.setString(5, event.getSenderAddress());
				stm.setString(6, event.getEventType());
				stm.setString(7, event.getEventContent());
				System.out.println("event " + event + " inserted");
				stm.executeUpdate();
				stm.close();
				c.close();
				
				
			} catch (Exception e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(0);
			}
		} else {
			System.out.println("DataBase - addEvent: event is null");
		}**/
	}

	/// creates a ArrayList composed of Event-Objects
	public ArrayList<Event> getEvents() throws ClassNotFoundException, SQLException {

		ArrayList<Event> events = new ArrayList<Event>();
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		Statement statement = null;
		connection = DriverManager.getConnection(dataBasePath);
		connection.setAutoCommit(false);
		try {

			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM EVENTS;");

			while (resultSet.next()) {
				Event event = new Event();
				event.setService(resultSet.getString("service"));
				event.setSender(resultSet.getString("sender"));
				event.setSenderAddress(resultSet.getString("senderAddress"));
				event.setEventType(resultSet.getString("eventType"));
				event.setEventContent(resultSet.getString("eventContent"));
				event.setEventOccuredTime(LocalDateTime.parse(resultSet.getString("eventOccuredTime")));
				event.setEventReceivedTime(LocalDateTime.parse(resultSet.getString("eventReceivedTime")));
				events.add(event);
			}

			resultSet.close();
			statement.close();
			connection.close();
			if (events.size() == 0) {
				System.out.println("no Events returned");
				return null;
			} else {
				return events;
			}

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("no Events returned");
		return null;
	}

	public ArrayList<Event> getLastEvents(int count) throws ClassNotFoundException, SQLException {
		ArrayList<Event> events = new ArrayList<Event>();
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		Statement statement = null;
		connection = DriverManager.getConnection(dataBasePath);
		connection.setAutoCommit(false);
		try {

			statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT * FROM EVENTS ORDER BY \"EventID\" DESC LIMIT " + count + ";");

			while (resultSet.next()) {
				Event event = new Event();
				event.setService(resultSet.getString("service"));
				event.setSender(resultSet.getString("sender"));
				event.setSenderAddress(resultSet.getString("senderAddress"));
				event.setEventType(resultSet.getString("eventType"));
				event.setEventContent(resultSet.getString("eventContent"));
				event.setEventOccuredTime(LocalDateTime.parse(resultSet.getString("eventOccuredTime")));
				event.setEventReceivedTime(LocalDateTime.parse(resultSet.getString("eventReceivedTime")));
				events.add(event);
			}

			resultSet.close();
			statement.close();
			connection.close();
			if (events.size() == 0) {
				System.out.println("no Events returned");
				return null;
			} else {
				return events;
			}

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("no Events returned");
		return null;
	}

	public ArrayList<Event> getEventsBySender(String sender) throws ClassNotFoundException, SQLException {
		ArrayList<Event> events = new ArrayList<Event>();
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		Statement statement = null;
		connection = DriverManager.getConnection(dataBasePath);
		connection.setAutoCommit(false);
		try {

			statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT * FROM EVENTS WHERE sender = '" + sender + "' ORDER BY \"EventID\" DESC;");

			while (resultSet.next()) {
				Event event = new Event();
				event.setService(resultSet.getString("service"));
				event.setSender(resultSet.getString("sender"));
				event.setSenderAddress(resultSet.getString("senderAddress"));
				event.setEventType(resultSet.getString("eventType"));
				event.setEventContent(resultSet.getString("eventContent"));
				event.setEventOccuredTime(LocalDateTime.parse(resultSet.getString("eventOccuredTime")));
				event.setEventReceivedTime(LocalDateTime.parse(resultSet.getString("eventReceivedTime")));
				events.add(event);
			}

			resultSet.close();
			statement.close();
			connection.close();
			if (events.size() == 0) {
				System.out.println("no Events returned");
				return null;
			} else {
				return events;
			}

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("no Events returned");
		return null;
	}

	public ArrayList<Event> getEventsByService(String service) throws ClassNotFoundException, SQLException {
		ArrayList<Event> events = new ArrayList<Event>();
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		Statement statement = null;
		connection = DriverManager.getConnection(dataBasePath);
		connection.setAutoCommit(false);
		try {

			statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT * FROM EVENTS WHERE service = '" + service + "' ORDER BY \"EventID\" DESC;");

			while (resultSet.next()) {
				Event event = new Event();
				event.setService(resultSet.getString("service"));
				event.setSender(resultSet.getString("sender"));
				event.setSenderAddress(resultSet.getString("senderAddress"));
				event.setEventType(resultSet.getString("eventType"));
				event.setEventContent(resultSet.getString("eventContent"));
				event.setEventOccuredTime(LocalDateTime.parse(resultSet.getString("eventOccuredTime")));
				event.setEventReceivedTime(LocalDateTime.parse(resultSet.getString("eventReceivedTime")));
				events.add(event);
			}

			resultSet.close();
			statement.close();
			connection.close();
			if (events.size() == 0) {
				System.out.println("no Events returned");
				return null;
			} else {
				return events;
			}

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("no Events returned");
		return null;
	}

	public ArrayList<Event> getEventsByType(String type) throws ClassNotFoundException, SQLException {
		ArrayList<Event> events = new ArrayList<Event>();
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		Statement statement = null;
		connection = DriverManager.getConnection(dataBasePath);
		connection.setAutoCommit(false);
		try {

			statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT * FROM EVENTS WHERE eventType = '" + type + "' ORDER BY \"EventID\" DESC;");

			while (resultSet.next()) {
				Event event = new Event();
				event.setService(resultSet.getString("service"));
				event.setSender(resultSet.getString("sender"));
				event.setSenderAddress(resultSet.getString("senderAddress"));
				event.setEventType(resultSet.getString("eventType"));
				event.setEventContent(resultSet.getString("eventContent"));
				event.setEventOccuredTime(LocalDateTime.parse(resultSet.getString("eventOccuredTime")));
				event.setEventReceivedTime(LocalDateTime.parse(resultSet.getString("eventReceivedTime")));
				events.add(event);
			}

			resultSet.close();
			statement.close();
			connection.close();
			if (events.size() == 0) {
				System.out.println("no Events returned");
				return null;
			} else {
				return events;
			}

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("no Events returned");
		return null;
	}

}
