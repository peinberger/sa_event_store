package de.ipa.saa.eventStore;

import java.time.LocalDateTime;

public class Event {
    private String service;
    private String sender;
    private String senderAddress;
    private String eventType;
    private String eventContent;
    private String eventOccuredTime;
    private String eventReceivedTime;
    
    public Event(String sender, String senderAddress, String eventType, String eventContent, String eventOccuredTime) {
        super();
        this.sender = sender;
        this.senderAddress = senderAddress;
        this.eventType = eventType;
        this.eventContent = eventContent;
        this.eventOccuredTime = eventOccuredTime;
        this.eventReceivedTime = LocalDateTime.now().toString();
    }

    public Event(){
    	this.eventReceivedTime = LocalDateTime.now().toString();
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderIP) {
        this.senderAddress = senderIP;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventContent() {
        return eventContent;
    }

    public void setEventContent(String eventContent) {
        this.eventContent = eventContent;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

	public String getEventReceivedTime() {
		return eventReceivedTime;
	}

	public void setEventReceivedTime(LocalDateTime eventReceivedTime) {
		this.eventReceivedTime = eventReceivedTime.toString();
	}
	
    
    public String getEventOccuredTime() {
        return eventOccuredTime;
    }

    public void setEventOccuredTime(LocalDateTime eventOccuredTime) {
        this.eventOccuredTime = eventOccuredTime.toString();
    }

}
