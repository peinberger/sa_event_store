package de.ipa.saa.eventStore;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;



public class CommandLineInterfaceHelper {
    private CommandLineParser parser;
    private Options options;
    
    public CommandLineInterfaceHelper(){
        
    }
    
    public CommandLine create(String[] args) throws ParseException{
        this.parser = new DefaultParser();
        this.options = new Options();
        
        
            Option cliAddress =   OptionBuilder.withArgName("address")
                                            .hasArg()
                                            .withDescription("define the zookeeper addrees")
                                            .create("address");
        Option cliLoaderPath = OptionBuilder.withArgName("loaderpath")
                                            .hasArg()
                                            .withDescription("define a specific loader path")
                                            .create("loaderpath");
        
        Option cliTesterPath = OptionBuilder.withArgName("testerpath")
                							.hasArg()
							                .withDescription("define a specific tester path")
							                .create("loaderpath");
        Option cliLoggerPath = OptionBuilder.withArgName("loggerpath")
							                .hasArg()
							                .withDescription("define a specific logger path")
							                .create("loaderpath");
        Option cliDebug = new Option("debug","show debug information");
        
        options.addOption(cliDebug);
        options.addOption(cliAddress);
        options.addOption(cliLoaderPath);
        options.addOption(cliTesterPath);
        options.addOption(cliLoggerPath);
        
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( "SenseAndAct-ComplexRuleEngine", options );
        
        CommandLine cmd = parser.parse( options, args);
        
        return cmd;
    }
    
    
}
