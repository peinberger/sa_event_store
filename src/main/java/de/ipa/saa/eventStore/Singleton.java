package de.ipa.saa.eventStore;

/// Singleton Pattern implementation

public class Singleton {
    private static Main mainInstance;
    private static DataBase dbInstance;
    
    private Singleton () {}

    /// Core Singleton Instance
    public static Main getCoreInstance () {
      if (Singleton.mainInstance == null) {
        Singleton.mainInstance = new Main ();
      }
      return Singleton.mainInstance;
    }
    
    /// DataBase Singleton Instance
    public static DataBase getDataBaseInstance () {
      if (Singleton.dbInstance == null) {
        Singleton.dbInstance = new DataBase ();
      }
      return Singleton.dbInstance;
    }
    
}
